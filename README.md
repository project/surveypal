DESCRIPTION
-----------
This module allows you to integrate Surveypal to your site. This means
you can show your Surveypal surveys on your Drupal website using your
personal API key.

HELP & SUPPORT
--------------
See http://developer.surveypal.com/docs/authentication-1 on how to get
your API key.

CREDITS
-------
- Creator of the module is Jussi Lindfors
  <jussi DOT lindfors AT druid DOT com>
- Current project maintainer is Marko Korhonen
  <marko DOT korhonen AT druid DOT com>
