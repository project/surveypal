<?php

class SurveypalAPI {
  private $_base_url = 'https://my.surveypal.com/api/rest/';
  private $_token = NULL;

  public function __construct($token) {
    $this->_token = $token;
  }

  public function surveys($published = NULL, $style_revision = NULL,
    $from = NULL, $to = NULL, $offset = NULL, $limit = NULL
  ) {
    $url_parts = array('surveys');
    return $this->_execute($url_parts);
  }

  public function survey($id) {
    $url_parts = array('survey', $id);
    return $this->_execute($url_parts);
  }

  private function _execute($url_parts, $get_params = array()) {
    // Create url for request
    $request_url = $this->_base_url . implode($url_parts, '/');
    if (!empty($get_params)) {
      $request_url .= '?' . http_build_query($get_params);
    }
    // Set curl options
    $curl = curl_init();
    $curl_options = array(
      CURLOPT_SSL_VERIFYPEER => FALSE,
      CURLOPT_RETURNTRANSFER => TRUE,
      CURLOPT_HTTPHEADER     => array(
        'Content-Type: application/json',
        'Accept: application/json',
        'X-Auth-Token: ' . $this->_token,
      ),
      CURLOPT_URL            => $request_url,
    );
    curl_setopt_array($curl, $curl_options);
    // Execute curl
    $result_json = curl_exec($curl);
    // Try to parse json
    $result = @json_decode($result_json);
    // Check result of curl
    $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
    // Prepare return object
    $return = (object) array(
      'success'     => FALSE,
      'result'      => $result,
      'status_code' => $status,
      'raw_result'  => $result_json,
    );
    // Check result of curl
    if ($status == 200) {
      $return->success = TRUE;
    }
    curl_close($curl);
    return $return;
  }
}
